" ====================
" init.vim
" 
" Initial customizations and loading of other configs
" ====================

" Tabs stuff
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent

" Options
set exrc
set guicursor=
set number
set relativenumber
set hidden
set noerrorbells
set termguicolors
set noshowmode
set cmdheight=2
set updatetime=50

" Searching
set nohlsearch
set incsearch

" Editing
set nowrap
set noswapfile
set nobackup
set undodir=~/.vim/undodir
set undofile
set scrolloff=8
set completeopt=menuone,noinsert,noselect
set colorcolumn=100
set signcolumn=yes

" Plugins
call plug#begin('~/.vim/plugged')

Plug 'gruvbox-community/gruvbox'

call plug#end()

" Ccolorscheme
colorscheme gruvbox
highlight Normal guibg=None

" Lets
let mapleader = " "

" Remaps
" nnoremap <leader>


