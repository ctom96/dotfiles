# Simple things that I want setup on every instance of zsh

# use nvim for editing commits and stuff
export EDITOR='nvim'
export VISUAL='nvim'

# nvm stuff                                                                                         
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
