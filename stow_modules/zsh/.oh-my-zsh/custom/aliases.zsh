########################################
# Aliases
########################################

# git
alias gits="git status"
alias gitc="git commit"
alias gita="git add -A"
alias gitl="git log"
alias gitlo="git log --oneline"
alias gitp="git push"
alias gitpsu="git push --set-upstream"
alias gitpu="git pull"

# vim
alias vim="nvim"
